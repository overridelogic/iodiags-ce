"""
IOdiags web proxy module.

This module provides the objects required for executing receiving and executing proxy
requests and responses on a HTTP enviroment.
"""

import ssl
import pipes
import queue
import urllib
import threading
from flask import Flask
from flask import Blueprint
from flask import request
from flask import Response
from gevent.wsgi import WSGIServer
from gevent.pool import Pool


class WebProxyResponse(object):
    """
    Response object for proxy requests.

    This class contains information about responses sent from the target to the connected
    clients.
    """

    def __init__(self, status_code, headers, data):
        """
        Initialize the response.

        :param status_code: the HTTP response status code.
        :param headers: a dictionary of response headers.
        :param data: the content of the response body.
        """
        assert isinstance(status_code, int) and isinstance(headers, dict) \
            and isinstance(data, bytes)

        self.status_code = status_code
        self.headers = headers
        self.data = data


class WebProxyRequest(object):
    """
    Request object for proxy requests.

    This class contains information about requests sent from the connected clients to the
    target, and their respective responses.
    """

    def __init__(self, method, url, headers, data, response=None):
        """
        Initialize the request.

        :param path: the requested URL.
        :param headers: a dictionary of request headers.
        :param data: the content of the request body.
        :param response: the response object.
        """
        assert isinstance(method, str) and isinstance(url, str) \
            and isinstance(headers, dict) and isinstance(data, bytes) \
            and (isinstance(response, WebProxyResponse) or response is None)

        self.method = method
        self.url = url
        self.headers = headers
        self.data = data
        self.response = response

    def as_curl(self):
        """
        Dump the request as a cURL command.

        :returns: a string containing the cURL command.
        """
        args = [
            '-X{0}'.format(self.method),
            pipes.quote(self.url),
        ]

        args += [
            "-H'{0}: {1}'".format(pipes.quote(k), pipes.quote(v))
            for k, v in self.headers.items()
        ]

        args.append("-d'{0}'".format(pipes.quote(str(self.data, 'utf-8'))))

        return 'curl {0}'.format(' \\\n  '.join(args))


class WebProxyHandler(object):
    """
    Web proxy handler.

    This class handles the proxy requests and application route configuation.
    """

    def __init__(self, target_uri):
        """
        Initialize the proxy.

        :param target_uri: the target URI for the proxy instance.
        :raises ValueError: if the URI is malformed.
        """
        assert isinstance(target_uri, str)

        self.target_uri = target_uri
        self.parsed = urllib.parse.urlparse(target_uri)

        if self.parsed.netloc == '':
            raise ValueError('no location supplied in target URI')

        self.request_options = {
            'add_prefix': [],
            'add_suffix': [],
            'add_header': [],
            'del_header': [],
            'processors': [],
        }

        self.response_options = {
            'add_prefix': [],
            'add_suffix': [],
            'add_header': [],
            'del_header': [],
            'processors': [],
        }

    def add_request_prefix(self, content):
        """
        Add a prefix to requests before sending them to the target.

        :param content: the content to add.
        """
        assert isinstance(content, bytes)
        self.request_options['add_prefix'].append(content)

    def add_request_suffix(self, content):
        """
        Add a suffix to requests before sending them to the target.

        :param content: the content to add.
        """
        assert isinstance(content, bytes)
        self.request_options['add_suffix'].append(content)

    def add_request_header(self, name, value):
        """
        Add a header to requests before sending them to the target.

        :param name: the header name.
        :param value: the header value.
        """
        assert isinstance(name, str) and isinstance(value, str)
        self.request_options['add_header'].append((name, value))

    def suppress_request_header(self, name):
        """
        Suppress a header from requests before sending them to the target.

        :param name: the header name.
        """
        assert isinstance(name, str)
        self.request_options['del_header'].append(name)

    def add_request_hook(self, func):
        """
        Add a preprocessing function for requests.

        :param func: the function to add.
        """
        assert hasattr(func, '__call__')
        self.request_options['processors'].append(func)

    def add_response_prefix(self, content):
        """
        Add a prefix to responses before sending them to the client.

        :param content: the content to add.
        """
        assert isinstance(content, bytes)
        self.response_options['add_prefix'].append(content)

    def add_response_suffix(self, content):
        """
        Add a suffix to responses before sending them to the client.

        :param content: the content to add.
        """
        assert isinstance(content, bytes)
        self.response_options['add_suffix'].append(content)

    def add_response_header(self, name, value):
        """
        Add a header to responses before sending them to the client.

        :param name: the header name.
        :param value: the header value.
        """
        assert isinstance(name, str) and isinstance(value, str)
        self.response_options['add_header'].append((name, value))

    def suppress_response_header(self, name):
        """
        Suppress a header from responses before sending them to the client.

        :param name: the header name.
        """
        assert isinstance(name, str)
        self.response_options['del_header'].append(name)

    def add_response_hook(self, func):
        """
        Add a preprocessing function for responses.

        :param func: the function to add.
        """
        assert hasattr(func, '__call__')
        self.response_options['processors'].append(func)

    def handle(self, method, path, headers, data, orig_request):
        """
        Handle a proxy request.

        :param method: HTTP method.
        :param path: the path requested.
        :param headers: a dictionary of request headers.
        :param data: the content of the request body.
        :param orig_request: the original request object.
        :returns: returns a proxy request object.
        """
        assert isinstance(path, str) and isinstance(headers, dict) and \
            isinstance(data, bytes) and isinstance(orig_request, (WebProxyRequest, type(None)))

        # Construct the URL
        request_url = '{0}://{1}{2}{3}'.format(
            self.parsed.scheme,
            self.parsed.netloc,
            self.parsed.path,
            path,
        )

        # Construct the request data
        request_data = bytes()
        for item in self.request_options['add_prefix']:
            request_data += item

        request_data += data

        for item in self.request_options['add_suffix']:
            request_data += item

        # Construct the request headers
        request_headers = {
            'Host': self.parsed.netloc,
            'Content-Length': str(len(request_data)),
        }

        for name, value in headers.items():
            if name in self.request_options['del_header'] or value == '':
                continue

            if name in ('Host', 'Content-Length'):
                continue

            request_headers[name] = value

        for name, value in self.request_options['add_header']:
            request_headers[name] = value

        # Create the proxy request
        proxy_request = WebProxyRequest(
            method,
            request_url,
            request_headers,
            request_data,
            None,
        )

        # Run the request hooks
        for func in self.request_options['processors']:
            func(self, proxy_request, orig_request)

        # Send the request and get the response
        lib_request = urllib.request.Request(proxy_request.url, proxy_request.data)
        lib_request.method = proxy_request.method
        for name, value in proxy_request.headers.items():
            lib_request.add_header(name, value)

        try:
            lib_response = urllib.request.urlopen(lib_request)
        except urllib.request.HTTPError as ex:
            lib_response = ex

        # Construct the response data
        output_data = lib_response.read()

        response_data = bytes()
        for item in self.response_options['add_prefix']:
            response_data += item

        response_data += output_data

        for item in self.response_options['add_suffix']:
            response_data += item

        # Construct the response headers
        response_headers = {}

        for name, value in lib_response.headers.items():
            if name in self.response_options['del_header'] or value == '':
                continue

            if name in ('Transfer-Encoding'):
                continue

            response_headers[name] = value

        for name, value in self.response_options['add_header']:
            response_headers[name] = value

        # Create the response object
        proxy_response = WebProxyResponse(
            lib_response.code,
            response_headers,
            response_data,
        )

        # Run the response hooks
        for func in self.response_options['processors']:
            func(self, proxy_response, proxy_request, orig_request)

        proxy_request.response = proxy_response
        return proxy_request

    def configure(self, app):
        """
        Configure a Flask app or blueprint to work with this proxy.

        :param app: the Flask application object.
        """
        @app.route('/<path:path>')
        @app.route('/', defaults={'path': ''})
        def proxy_route(path):
            root_path = str(request.url_rule).replace('<path:path>', '').rstrip('/')
            input_path = request.path[len(root_path):]
            input_headers = dict(request.headers)
            input_data = request.get_data()

            orig_request = WebProxyRequest(
                request.method,
                request.url,
                input_headers,
                input_data,
            )

            proxy_request = self.handle(
                request.method,
                input_path,
                input_headers,
                input_data,
                orig_request,
            )

            proxy_response = proxy_request.response

            response = Response()
            response.status_code = proxy_response.status_code
            response.headers = proxy_response.headers
            response.data = proxy_response.data
            return response


class WebProxyContainer(object):
    """
    Container objects for proxies.

    This class handles application creation and the event loop for each proxy container.
    """

    def __init__(self, port, use_ssl=False, poolsize=1):
        """
        Initialize the container.

        :param port: the port on which to listen.
        :param use_ssl: whether to use SSL requests and responses.
        :param poolsize: the number of workers for the container.
        """
        assert isinstance(port, int) and port >= 0 and isinstance(use_ssl, bool) \
            and isinstance(poolsize, int) and poolsize >= 1

        self.port = port
        self.use_ssl = use_ssl
        self.poolsize = poolsize
        self.thread = None
        self.events = queue.Queue()
        self.handlers = []

    def is_alive(self):
        """
        Check if the container is alive.

        :returns: True if the container is running, False otherwise.
        """
        if self.thread is not None and self.thread.is_alive():
            return True

        self.thread = None
        return False

    def run(self):
        """
        Run the container.
        """
        wsgi = Flask(
            'webproxy-{0}'.format(self.port),
            static_url_path='/////',
        )

        for item in self.handlers:
            path, handler = item.copy().popitem()
            blueprint = Blueprint(path, path, static_url_path='/////')
            handler.configure(blueprint)
            wsgi.register_blueprint(blueprint, url_prefix=path.rstrip('/'))

        pool = Pool(self.poolsize)

        kwargs = {}
        if self.use_ssl:
            kwargs['ssl_context'] = ssl.create_default_context()

        server = WSGIServer(('', self.port), wsgi, spawn=pool, **kwargs)

        server.start()
        while True:
            server._stop_event.wait(0.05)

            try:
                event = self.events.get_nowait()
                if event == 'stop':
                    server.stop()
                    break

            except queue.Empty:
                pass

    def start(self):
        """
        Start the container.
        """
        if self.is_alive():
            return

        self.events = queue.Queue()
        self.thread = threading.Thread(target=self.run)
        self.thread.start()

    def stop(self, timeout=None):
        """
        Stop the container.

        :param timeout: the timeout, in seconds for which to wait for the container
        thread to join.
        :returns: True if the container has stopped, False otherwise.
        """
        if not self.is_alive():
            return

        self.events.put_nowait('stop')
        self.thread.join(timeout)
        return self.is_alive()

    def restart(self):
        """
        Restart the container.
        """
        self.stop()
        self.start()

    def attach(self, handler, path):
        """
        Attach a handler to a path.

        :param handler: the web proxy handler object.
        :param path: the path to attach to.
        """
        assert isinstance(handler, WebProxyHandler) and isinstance(path, str)
        path = path.rstrip('/') + '/'

        for item in self.handlers:
            saved_path, saved_handler = item.copy().popitem()
            if saved_path.startswith(path):
                raise ValueError('path {0} conflicts with {1}'.format(path, saved_path))

        self.handlers.append({path: handler})

    def detach(self, path):
        """
        Detach a handler from a path.

        :param path: the path to detach from.
        """
        assert isinstance(path, str)
        path = path.rstrip('/') + '/'

        for item in self.handlers:
            saved_path, saved_handler = item.copy().popitem()
            if saved_path == path:
                self.handlers.remove(item)
                return

        raise ValueError('path {0} is not attached to a handler'.format(path))
