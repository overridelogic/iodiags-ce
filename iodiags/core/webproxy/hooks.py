"""
IOdiags Web Proxy hooks.

This module provides common hooks that can be used with web proxy handlers.
"""

import re
import urllib


def response_rewrite_html_paths(handler, response, request, orig_request):
    """
    Rewrite the paths in the response data according to the mount path

    :param handler: the web proxy handler object.
    :param response: the target response object.
    :param request: the target request object.
    :param orig_request: the original client request object.
    """
    try:
        if 'text/html' not in request.headers['Content-Type'] and \
           'application/xhtml+xml' not in request.headers['Content-Type']:
            return
    except KeyError:
        pass

    request_path = urllib.parse.urlparse(request.url).path
    orig_request_path = urllib.parse.urlparse(orig_request.url).path
    root_path = orig_request_path[:-len(request_path)]

    response.data = re.sub(
        b'([^=]+)=(["\'])(/[^/])',
        b'\\1=\\2' + bytes(root_path, 'ascii') + b'\\3',
        response.data,
    )
    response.headers['Content-Length'] = len(response.data)
