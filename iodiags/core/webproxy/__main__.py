"""
IOdiags web proxy module.

This module is a console entry point to spawn single web proxy containers.
"""

import pydoc
import argparse

from iodiags.core.webproxy import WebProxyContainer
from iodiags.core.webproxy import WebProxyHandler


def main():
    """
    Run a web proxy instance from the command line.
    """
    def run(host_port, mount, use_ssl=False, poolsize=4,
            add_request_headers=[], del_request_headers=[], request_hooks=[],
            add_response_headers=[], del_response_headers=[], response_hooks=[]):
        """
        Start an instance of the web proxy server.

        :param host_port: the port to listen on.
        :param mount: a list of URLs and paths to mount.
        :param use_ssl: whether to start the server with SSL or not.
        :param poolsize: the size of the worker pool.
        :param add_request_headers: a dictionary of headers to add to the request.
        :param del_request_headers: a list of headers to suppress from the request.
        :param request_hook: a list of hooks to add to the request.
        :param add_response_headers: a dictionary of headers to add to the response.
        :param del_response_headers: a list of headers to suppress from the response.
        :param response_hook: a list of hooks to add to the response.
        """
        container = WebProxyContainer(host_port, use_ssl, poolsize)

        for mount_point in mount:
            parts = mount_point.split('::', 1)
            url = parts[0]
            path = parts[1] if len(parts) > 1 else '/'

            handler = WebProxyHandler(url)

            for raw in add_request_headers:
                name, value = raw.split(':', 1)
                handler.add_request_header(name, value)

            for name in del_request_headers:
                handler.suppress_request_header(name)

            for name in request_hooks:
                obj = pydoc.locate(name)
                if obj is None:
                    raise NameError('hook {0} is not importable'.format(name))
                handler.add_request_hook(obj)

            for raw in add_response_headers:
                name, value = raw.split(':', 1)
                handler.add_response_header(name, value)

            for name in del_response_headers:
                handler.suppress_response_header(name)

            for name in response_hooks:
                obj = pydoc.locate(name)
                if obj is None:
                    raise NameError('hook {0} is not importable'.format(name))
                handler.add_response_hook(obj)

            container.attach(handler, path)

        container.start()
        container.thread.join()

    parser = argparse.ArgumentParser(
        prog='iodiags-webproxy',
        description='Start an IOdiags web proxy instance.',
    )
    parser.add_argument('host_port', metavar='HOST_POST', type=int,
                        help='the port to listen on')
    parser.add_argument('mount', nargs='+', metavar='URL[::PATH]',
                        help='the target URL. Can be specified multiple times if the '
                             'source path is supplied')
    parser.add_argument('-s', '--poolsize', type=int,
                        help='the number of workers to run the API application with')
    parser.add_argument('-S', '--use-ssl', action='store_true',
                        help='start the server with SSL support instead of plain HTTP')
    parser.add_argument('-H', '--add-request-headers', nargs='+',
                        help='a list of request headers to add to every request')
    parser.add_argument('-D', '--del-request-headers', nargs='+',
                        help='a list of request headers to suppress from every request')
    parser.add_argument('-P', '--request-hooks', nargs='+',
                        help='a list of hooks to execute for every request')
    parser.add_argument('-A', '--add-response-headers', nargs='+',
                        help='a list of response headers to add to every response')
    parser.add_argument('-X', '--del-response-headers', nargs='+',
                        help='a list of response headers to suppress from every response')
    parser.add_argument('-R', '--response-hooks', nargs='+',
                        help='a list of hooks to execute for every request')
    args = parser.parse_args()
    kwargs = dict(filter(lambda x: x[1] is not None, list(args.__dict__.items())))

    run(**kwargs)


if __name__ == '__main__':
    main()
