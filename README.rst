IOdiags: Software Development Diagnostics
#########################################

IOdiags is a software development diagnostics tool suite. Its goal is to
provide software developers with tools to debug, analyze, trace and troubleshoot
software applications during their development.

This software is still under heavy development, and is available for preview.
Contributions are welcome.


.. contents::

.. section-numbering::


Features
========

Current features
----------------

These are currently implemented features:

* Complete HTTP and HTTPS proxy
* Request and response manipulation
* Mount different URLs on the same port at different paths


Upcoming features
-----------------

These are features actively under deployment:

* Log HTTP requests and responses and trace them
* Diagnostics server
* Logging API
* Socket proxying
* Log socket connections, I/O and more
* Web interface for diagnostics servers


Planned features
----------------

These features are part of the roadmap, but are not being worked on at this time:

* Diagnostics clusters
* Mobile applications (Android, etc.)
* Desktop applications (Linux, Mac OS, Windows)


Installation
============

A universal installation method (that works on Windows, Mac OS X, Linux, …,
and always provides the latest version) is to use ``pip``:


.. code-block:: bash

    # Make sure we have an up-to-date version of pip and setuptools:
    $ pip install --upgrade pip setuptools

    $ pip install --upgrade iodiags


(If ``pip`` installation fails for some reason, you can try
``easy_install iodiags`` as a fallback.)


Python version
--------------

Although Python 3.3 and 3.4 should work, all development is done on Python 3.5.
As such, only 3.5 and newer is currently officially supported.


Unstable version
----------------

You can also instead of the latest the latest unreleased development version
directly from the ``develop`` branch on GitLab.
It is a work-in-progress of a future stable release so the experience
might be not as smooth.

With ``pip``:

.. code-block:: bash

    $ pip install --upgrade 'https://gitlab.com/overridelogic/iodiags-ce/repository/archive.tar.gz?ref=develop'


Usage
=====

Simple Web Proxy
----------------

.. code-block:: bash

    $ iodiags-webproxy 8080 https://www.python.org


Synopsis:

.. code-block:: bash

    $ iodiags-webproxy [flags] HOST_PORT TARGET_URL[:MOUNT_PATH]


See also ``iodiags-webproxy --help``.


Examples
========

Simple Web Proxy running on ``http://localhost:8080`` pointing to ``www.python.org``:

.. code-block:: bash

    $ iodiags-webproxy 8080 https://www.python.org


Same, but started as a Python module:

.. code-block:: bash

    $ python -m iodiags.core.webproxy 8080 https://www.python.org


Complex Web Proxy running on ``http://localhost:8080``, with ``/pypi`` pointing to
``pypi.org`` and ``/python`` to ``www.python.org``:

.. code-block:: bash

    $ iodiags-webproxy 8080 \
        https://pypi.org::/pypi \                                     # mount /pypi
        https://www.python.org::/python \                             # mount /python
        -D'Accept-Encoding' \                                         # disable compression
        -R'iodiags.core.webproxy.hooks.response_rewrite_html_paths'   # rewrite images/links


Related projects
----------------

Dependencies
~~~~~~~~~~~~

Under the hood, IOdiags uses these amazing libraries:

* `Flask <http://flask.pocoo.org/>`_
  — A Python microframework
* `gevent <http://gevent.org/>`_
  — A coroutine -based Python networking library


Alternatives
~~~~~~~~~~~~

* `Fiddler <http://www.telerik.com/fiddler>`_ — a proprietary, but free, web
  debugging proxy.


About the project
=================

Change log
----------

See `CHANGELOG <https://gitlab.com/overridelogic/iodiags-ce/blob/master/CHANGELOG.rst>`_.


Licence
-------

MIT License: see `LICENSE <https://gitlab.com/overridelogic/iodiags-ce/blob/master/LICENSE>`_.



Authors
-------

**Francis Lacroix** `@netcoder1` created IOdiags while at **OverrideLogic**.
