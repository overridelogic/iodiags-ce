import unittest
from flask import Flask
from gevent.wsgi import WSGIServer

from iodiags.core import webproxy


class WebProxyHandlerTests(unittest.TestCase):
    def test_invalid_url(self):
        with self.assertRaises(ValueError):
            proxy = webproxy.WebProxyHandler('http:432')

    def test_valid_request(self):
        proxy = webproxy.WebProxyHandler('https://mit-license.org')
            
        proxy.add_request_header('User-Agent', 'curl')
        proxy.add_request_prefix(b'foo')
        proxy.add_request_suffix(b'bar')
        proxy.suppress_request_header('Accept-Encoding')
        proxy.add_request_hook(lambda h, r, o: setattr(r, 'data', r.data.replace(b'oo', b'aa')))

        proxy.add_response_header('X-Response-Test', 'true')
        proxy.add_response_prefix(b'hello')
        proxy.add_response_suffix(b'world')
        proxy.suppress_response_header('ETag')
        proxy.add_response_hook(lambda h, r, i, o: setattr(r, 'data', r.data.replace(b'MIT', b'Hello')))

        request = proxy.handle('GET', '/license.txt', {'Hey': 'There'}, b'test', None)
        response = request.response
        self.assertEqual(request.method, 'GET')
        self.assertEqual(request.url, 'https://mit-license.org/license.txt')
        self.assertEqual(request.headers, {
            'Content-Length': '10',
            'User-Agent': 'curl',
            'Hey': 'There',
            'Host': 'mit-license.org',
        })
        self.assertEqual(request.data, b'faatestbar')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['X-Response-Test'], 'true')
        self.assertFalse('ETag' in response.headers)
        self.assertEqual(response.data[0:30], b'helloThe Hello License (Hello)')
        self.assertEqual(response.data[-10:], b'ARE.\nworld')
