#!/usr/bin/python3

from setuptools import setup
from setuptools import find_packages

import iodiags


setup(
    name='iodiags',
    version=iodiags.__version__,

    description=iodiags.__doc__.split('\n')[1].strip(),
    long_description=iodiags.__doc__.strip(),
    url='http://www.diags.io',
    author='OverrideLogic',
    author_email='info@overridelogic.com',
    maintainer='Francis Lacroix',
    maintainer_email='f@overridelogic.com',

    license='MIT',
    platforms=['any'],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Information Technology',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Topic :: Internet :: Proxy Servers',
        'Topic :: Internet :: Log Analysis',
    ],

    packages=find_packages(exclude=['tests', 'tests.*']),
    provides=['iodiags'],

    python_requires='>=3.5',
    setup_requires=['flake8>=3.2.1'],
    tests_require=[],
    test_suite='tests',

    install_requires=[
        'flask>=0.12',
        'Werkzeug>=0.11.15',
        'gevent>=1.2.1',
        'greenlet>=0.4.12',
    ],

    entry_points={
        'console_scripts': [
            'iodiags-webproxy=iodiags.core.webproxy.__main__:main',
        ],
    },
)
