Change Log
==========

This document records all notable changes to **IOdiags**.
This project adheres to `Semantic Versioning <http://semver.org/>`_.


`0.1.1 Allis`_
-------------------------
* Fixed setup script to exclude tests.

`0.1.0 Allis`_
-------------------------
* Added web proxy handlers, containers, requests and responses.
* Added ``iodiags.core.webproxy`` as runnable module.
* Added ``response_rewrite_html_paths`` web proxy hook.
